#!/usr/bin/python
import configparser
import datetime

import requests
from telegram.ext import Updater, CommandHandler, Filters, MessageHandler
from io import BytesIO
from PIL import Image


from weather_service import get_weather_info_from_observatory, process_rain_graph, get_typhoon_info

config = configparser.ConfigParser()
config.read('config.ini')
token = config.get("CONFIG", "token")
updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher


def polling_multiple(update, context):
    chat_id = update.effective_chat.id
    if not len(context.args):
        return context.bot.send_message(
            chat_id=chat_id,
            text="入野先啦仆街")
    question, *questions = context.args
    message = context.bot.send_poll(
        update.effective_chat.id,
        question,
        questions,
        is_anonymous=False,
        allows_multiple_answers=True
    )
    payload = {
        message.poll.id: {
            "questions": questions,
            "message_id": message.message_id,
            "chat_id": update.effective_chat.id,
            "answers": 0,
        }
    }
    context.bot_data.update(payload)
    context.bot.pinChatMessage(chat_id=chat_id, message_id=message.message_id)


def polling(update, context):
    print(update.effective_user)
    chat_id = update.effective_chat.id
    if len(context.args) < 3:
        return context.bot.send_message(
            chat_id=chat_id,
            text="入野先啦仆街 ".format(update.effective_user.mention_markdown()))
    question, *questions = context.args
    message = context.bot.send_poll(
        update.effective_chat.id,
        question,
        questions,
        is_anonymous=False,
    )
    payload = {
        message.poll.id: {
            "questions": questions,
            "message_id": message.message_id,
            "chat_id": update.effective_chat.id,
            "answers": 0,
        }
    }
    context.bot_data.update(payload)
    context.bot.pinChatMessage(chat_id=chat_id, message_id=message.message_id)


def typhoon(update, context):
    typhoon_info_list = get_typhoon_info()
    for info in typhoon_info_list:
        print(info)
        res = requests.get(info["typhoon_img_url"])
        img = BytesIO(res.content)
        context.bot.send_photo(update.effective_chat.id, img, info["name"])


def weather(update, context):
    today_weather = get_weather_info_from_observatory()
    process_rain_graph()
    context.bot.send_message(chat_id=update.effective_chat.id, text=today_weather, parse_mode="MarkdownV2")
    context.bot.send_animation(update.effective_chat.id, open('./radar.gif', 'rb'))


def on9(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="on9")


def message_handler(bot, update):
    print(bot)
    print(update.bot_data)
    if bot.message.text == "@grammarho57":
        update.bot.send_message(chat_id=bot.message.chat.id, text="on9 發仔")
    if bot.message.text == "莫忽":
        update.bot.send_message(chat_id=bot.message.chat.id, text="on9 莫忽")


if __name__ == '__main__':
    print("bot start", datetime.datetime.now())
    polling_handler = CommandHandler('polling', polling)
    polling_multiple_handler = CommandHandler('polling_multiple', polling_multiple)
    weather_handler = CommandHandler('weather', weather)
    typhoon_handler = CommandHandler('typhoon', typhoon)
    on9_handler = CommandHandler('on9', on9)
    message_handler = MessageHandler(Filters.text, message_handler)
    dispatcher.add_handler(typhoon_handler)
    dispatcher.add_handler(polling_handler)
    dispatcher.add_handler(polling_multiple_handler)
    dispatcher.add_handler(weather_handler)
    dispatcher.add_handler(on9_handler)
    dispatcher.add_handler(message_handler)
    updater.start_polling()
    updater.idle()
